<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Exceptions\TokenAuthenticationException;

/**
 * Adds the authorized user id to the request so it can be accessed in controllers.
 */
class TokenAuthentication
{
		/**
		 * Handle an incoming request.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @param  \Closure  $next
		 * @return mixed
		 */
		public function handle($request, Closure $next)
		{
			$token = $request->bearerToken();
			if ($token) {
				$user = User::where('token', $token)->get()->first();
				if ($user) {
					$request->attributes->add(['user_id' => $user->id]);

					return $next($request);
				}

				throw new TokenAuthenticationException($token);
			}

			throw new TokenAuthenticationException();
		}
}
