<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
	public function authenticate(Request $request)
	{
		$user = User::find($request->user_id);
		if ($user) {
			// Update the user's token if needed
			if ($user->token != $request->access_token) {
				$user->token = $request->access_token;
				$user->save();
			}
		}
		else
		{
			// Create a new user
			$user = new User;
			$user->id = $request->user_id;
			$user->token = $request->access_token;
			$user->save();
		}
	}
}
