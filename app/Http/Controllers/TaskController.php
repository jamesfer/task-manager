<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Exceptions\ResourceNotFoundException;

class TaskController extends Controller
{
  /**
   * Returns the start of a query with the user_id filter applied.
   */
  private function getTasks(Request $request)
  {
    return Task::where('user_id', $request->get('user_id'));
  }

  /**
   * Attempts to fetch a task from the database with the given id.
   * If it can't be found, throws a ResourceNotFoundException.
   */
  private function findTask(Request $request, $id)
  {
    $task = $this->getTasks($request)->where('id', $id)->get()->first();
    if ($task) {
      return $task;
    }
    else {
      throw new ResourceNotFoundException("Task", ['id' => $id]);
    }
  }

  /**
   * Lists all tasks in the database associated with a user.
   */
  public function listAll(Request $request)
  {
    $results = $this->getTasks($request)
      ->orderby('created_at', 'desc')
      ->get();
    return response()->json($results);
  }

  /**
   * Returns a single Task.
   */
  public function get($id)
  {
    return response()->json($this->findTask($id));
  }

  /**
   * Creates a new Task.
   */
  public function create(CreateTaskRequest $request)
  {
    $newTask = new Task;
    $newTask->name = $request->name;
    $newTask->user_id = $request->get('user_id');
    $newTask->save();
    return response()->json($newTask);
  }

  /**
   * Updates a Task.
   */
  public function update(StoreTaskRequest $request, $id)
  {
    $task = $this->findTask($request, $id);
    if (isset($request->name)) {
      $task->name = $request->name;
      $task->save();
    }
    return response()->json($task);
  }

  /**
   * Deletes a Task.
   */
  public function delete(Request $request, $id)
  {
    $this->findTask($request, $id)->delete();
  }
}
