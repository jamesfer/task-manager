<?php

namespace App\Http\Requests;

use App\Http\Requests\UtilityFormRequest;

/**
 * Ensures that the parameters for the new task are valid.
 * Used when updating a task.
 */
class StoreTaskRequest extends UtilityFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255'
        ];
    }
}
