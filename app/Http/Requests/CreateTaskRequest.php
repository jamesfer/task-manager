<?php

namespace App\Http\Requests;

use App\Http\Requests\StoreTaskRequest;

/**
 * Builds on top of the StoreTaskRequest by also making the name parameter required.
 * Used when creating a new task.
 */
class CreateTaskRequest extends StoreTaskRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return $this->combine(parent::rules(), [
          "name" => "required"
      ]);
    }
}
