<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

/**
 * Provides a simple utility function for combining validation rules from multiple sources.
 */
class UtilityFormRequest extends FormRequest
{
    /**
     * Combines two validation arrays.
     * Duplicate entries will be appended using a |.
     */
    public function combine($original, $extension)
    {
      foreach ($extension as $key => $value) {
        if (array_key_exists($key, $original)) {
          $original[$key] = $original[$key] . "|" . $value;
        }
        else {
          $original[$key] = $value;
        }
      }
      return $original;
    }
}
