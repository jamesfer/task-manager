<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use \Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        // \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      if ($request->wantsJson())
      {
        return $this->renderJsonException($request, $exception);
      }

      return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }


    /**
     * Returns a json response with these properties:
     *   error: Exception class name
     *   httpCode: Exception code, also used as the http response code
     *   message: Exception message
     *   data: Any additional data supplied with the exception. Only
     *        added if needed.
     */
    protected function renderJsonException($request, Exception $exception)
    {
      // echo $this->stripNamespace(get_class($exception));
      $responseBody = [
  			'error' => $this->stripNamespace(get_class($exception)),
  			'httpCode' => $exception->getCode(),
  			'message' => $exception->getMessage()
  		];

      if ($exception->getLine()) {
        $responseBody['lineNumber'] = $exception->getLine();
      }
      if ($exception->getFile()) {
        $responseBody['file'] = $exception->getFile();
      }

      if ($exception instanceof ValidationException) {
        $responseBody['httpCode'] = 422;
        $responseBody['data'] = $exception->validator->errors()->getMessages();
      }
      else if ($exception instanceof GeneralException && $exception->getData()) {
        $responseBody['data'] = $exception->getData();
      }
      // Make sure the response code is valid
      else if ($responseBody['httpCode'] < 100 || $responseBody['httpCode'] > 599) {
        $responseBody['code'] = $responseBody['httpCode'];
        $responseBody['httpCode'] = 500;
      }

      return response()->json($responseBody, $responseBody['httpCode']);
    }


    /**
     * Strips all namespace information out of a class name.
     * Returns just the name of the class.
     */
    protected function stripNamespace($className)
    {
      $substrStartPos = strrpos($className, '\\');
      if ($substrStartPos > 0) {
        $substrStartPos += 1;
      }
      return substr($className, $substrStartPos);
    }
}
