<?php

namespace App\Exceptions;

class TokenAuthenticationException extends GeneralException
{
	public function __construct($token = null)
	{
		if ($token) {
			parent::__construct(401, "Invalid token header provided", [
				"token" => $token
			]);
		}
		else {
			parent::__construct(401, "Missing authorization bearer header");
		}
	}
}
