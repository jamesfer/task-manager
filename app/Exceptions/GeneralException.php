<?php

namespace App\Exceptions;

use RuntimeException;


/**
 * Adds a data member to the standard exception so that additional
 * information can be sent in the response.
 */
class GeneralException extends RuntimeException
{
	protected $data;


	public function __construct($httpCode, $message, $data = null)
	{
		$this->code = $httpCode;
		$this->message = $message;
		$this->data = $data;
	}

	public function getData()
	{
		return $this->data;
	}
}
