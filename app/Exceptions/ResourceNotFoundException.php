<?php

namespace App\Exceptions;

class ResourceNotFoundException extends GeneralException
{
	protected $resourceName;


	public function __construct($resourceName, $data = null)
	{
		$this->resourceName = $resourceName;
		$message = $resourceName . " could not be found.";
		parent::__construct(400, $message, $data);
	}
}
