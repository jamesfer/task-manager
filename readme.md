# Task Manager

## Installation

Download the source code from Bitbucket then cd into the task-manager directory:
```
$ git clone https://jamesfer@bitbucket.org/jamesfer/task-manager.git
$ cd task-manager
```
Make sure to copy the .env file into the project manually.
Install JS and PHP packages:
```
$ composer install
$ npm install
```
Run Gulp to update the public directory:
```
$ gulp
```
Create the `task_manager` database in MySQL or edit the .env file to point to a different database.
Then apply all the migrations
```
$ php artisan migrate
```
Start the development server:
```
$ php artisan serve
```

## A note on URLs

This project uses OAuth supplied by Google to track users. The current settings only allow requests from localhost:8000, all others will be blocked by Google. If you need to run the code from another URL, let me know and I'll add it to the whitelist of the app.
