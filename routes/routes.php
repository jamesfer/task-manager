<?php

use Illuminate\Http\Request;



/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file will include the route definitions from other php files.
|
*/

Route::group([
  'prefix' => 'api/tasks',
  'middleware' => ['tokenAuthentication']
], function ($router) {
  require base_path('routes/tasks.php');
});

Route::group([
  'prefix' => 'api/auth'
], function($router) {
  require base_path('routes/auth.php');
});

Route::group([], function($router) {
  require base_path('routes/web.php');
});
