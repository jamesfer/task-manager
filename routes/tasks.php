<?php

use Illuminate\Http\Request;


// Task routes
// The create route was changed to put to avoid a bug in PHP 5.6
Route::get('/', 'TaskController@listAll');
Route::get('/{id}', 'TaskController@get');
Route::put('/', 'TaskController@create');
Route::put('/{id}', 'TaskController@update');
Route::delete('/{id}', 'TaskController@delete');
