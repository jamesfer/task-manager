<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Task Manager</title>

    <link rel="stylesheet" href="css/app.css">

  </head>

  <body>
    <div class="container" id="app">
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
          <h1>Task Manager</h1>

          <login v-if="!authState.apiReady"></login>
          <task-list v-if="authState.apiReady"></task-list>
        </div>
      </div>

      <toasts></toasts>
    </div>
    <script src="js/app.js"></script>
  </body>
</html>
