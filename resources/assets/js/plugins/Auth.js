// Urls
var loginUrl = "https://accounts.google.com/o/oauth2/v2/auth";
var validationUrl = "https://www.googleapis.com/oauth2/v2/tokeninfo";
var authenticationUrl = "/api/auth/";


// Private details about the app
var clientId, redirectUri;


class AuthService {
	constructor(Vue, options) {
		this.Vue = Vue;

		clientId = options.clientId;
		redirectUri = options.redirectUri;

		this.state = {
			// True when the access token has been validated and requests
			// to the api can be made
			apiReady: false,

			// True when the service begins to validate a token.
			// This can be used to display a loading icon if needed.
			validatingToken: false,

			// The access token object
			accessToken: null,

			// The user object
			user: null,

			// Any errors that pop up during the login flow
			// They are stored as booleans so that the Login component
			// can handle them as it wishes.
			errors: {
				userDeniedLogin: false,
				invalidToken: false,
				mismatchedAudience: false
			}
		};

		this.continueLoginFlow();
	}


	// Continues the login flow from where ever we left off.
	continueLoginFlow() {
		// Check if there is a previous token stored in the session
		var sessionToken = this.getSessionToken();
		if (sessionToken) {
			console.log('Session token found');
			this.deleteSessionToken();
			this.validateToken(sessionToken);
		}
		else {
			var params = this.getHashObject();
			// Check if there is a denied reponse from google
			if (this.wasRequestDenied(params)) {
				this.state.errors.userDeniedLogin = true;
			}
			else {
				// Check if there is a successful response from google
				var accessToken = this.extractAccessToken(params);
				if (accessToken) {
					this.validateToken(accessToken);
				}
			}
		}
	}


	// Attempts to parse the access token from session storage
	// Returns null if there is none.
	getSessionToken() {
		if (sessionStorage && sessionStorage.accessToken) {
			return JSON.parse(sessionStorage.accessToken);
		}
		return null;
	}


	// Deletes the token from session storage
	deleteSessionToken() {
		if (sessionStorage && sessionStorage.accessToken) {
			sessionStorage.removeItem('accessToken');
		}
	}


	// Confirms that the access token is real
	validateToken(accessToken) {
		console.log('Validating token...');
		this.state.validatingToken = true;
		return this.getTokenInfoFromGoogle(accessToken)
			.catch((response) => {
				console.log('Google denied the token');
				console.log(response.body);
				this.state.errors.invalidToken = true;
				return Promise.reject(response);
			})
			.then((response) => {
				// Confirm the response is for this app
				if (response.body.audience != clientId) {
					console.log('Mismatched audience');
					this.state.errors.mismatchedAudience = true;
					return Promise.reject({
						error: 'mismatchedAudience',
						response: response.body
					});
				}
				else {
					var user = {
						id: response.body.user_id,
						scopes: response.body.scope
					};

					return this.validateTokenAndUser(accessToken, user);
				}
			})
			.catch((response) => {
				// If the token was rejected at any point,
				// reset the state of the login flow
				this.resetState();
			});
	}


	// Validates the token through the google endpoint
	getTokenInfoFromGoogle(accessToken) {
		return Vue.http.get(validationUrl, {params: {access_token: accessToken.value}});
	}


	// Sends both the user id and the token to our backend so they can
	// be used for api requests.
	validateTokenAndUser(accessToken, user) {
		console.log('Validating token and user...');
		return Vue.http.put(authenticationUrl, {
			access_token: accessToken.value,
			user_id: user.id
		})
			.then((response) => {
				console.log('Login successful');
				this.state.accessToken = accessToken;
				this.state.user = user;
				this.state.apiReady = true;

				sessionStorage.accessToken = JSON.stringify(accessToken);
			})
			.catch((response) => {
				console.log('Authentication failed');
				console.log(response);
			});
	}


	// Resets the auth state as if the user had never started the login flow.
	resetState() {
		console.log('Reseting state...');
		this.state.apiReady = false;
		this.state.validatingToken = false;
		this.state.accessToken = null;
		this.state.user = null;
	}


	// Returns true if the request for the access token was denied
	wasRequestDenied(params) {
		return params.error === 'access_denied';
	}


	// Extracts the access_token details from the params object and
	// returns it. If there isn't token details, will return null.
	extractAccessToken(params) {
		if (params.access_token && params.token_type && params.expires_in)
		{
			return {
				value: params.access_token,
				type: params.token_type,
				expiresIn: params.expires_in
			};
		}
		return null;
	}


	// Begins the login process.
	// Note: this will cause the page to be redirected to a different
	// location.
	login(scopes) {
		if (!this.state.user) {
			window.location = this.makeUrl(scopes);
		}
	}


	// Converts the query parameters of the current location into
	// an object.
	// Implementation from:
	// https://developers.google.com/identity/protocols/OAuth2UserAgent
	getHashObject() {
		var params = {};
		if (location.hash.length > 0) {
			var queryString = location.hash.substring(1),
				regex = /([^&=]+)=([^&]*)/g,
				match;
			while (match = regex.exec(queryString)) {
				params[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);
			}
		}
		return params;
	}


	// Constructs the OAuth url
	makeUrl(scope) {
		return Vue.url(loginUrl, {
			response_type: 'token',
			client_id: clientId,
			redirect_uri: redirectUri,
			scope
		});
	}
}


export default {
	install(Vue, options) {
		Vue.auth = new AuthService(Vue, options);

		// Store the auth state so it can be used in mixins
		var globalAuthState = Vue.auth.state;

		// Attach a mixin to the global Vue instance for components to use
		Vue.authMixin = {
			data() {
				return {
					authState: globalAuthState
				};
			}
		};

		// Install the auth service as an instance property
		Vue.prototype.$auth = Vue.auth;

		// Interceptor that will automatically add the Authorization
		// header if there is a valid token.
		Vue.http.interceptors.push((request, next) => {
			if (Vue.auth.state.apiReady) {
				request.headers.append('Authorization', 'Bearer ' + Vue.auth.state.accessToken.value);
			}
			return next();
		});
	}
}
