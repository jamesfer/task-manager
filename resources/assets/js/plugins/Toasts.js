export default {
	install(Vue, options) {
		var shouldStoreToasts = true;
		var storedToasts = [];

		// Used before the toast component has been registered to store toasts
		var storeToast = function(type, content) {
			storedToasts.push({type, content});
		};

		// Broadcasts the toast to the toast component
		var emitToast = function(type, content) {
			this.$root.$emit('toast', type, content);
		}

		// Switches from storing the toasts to emiting the toasts.
		var switchToastMethod = function(vueInstance) {
			// Broadcast all the stored up toasts.
			while (storedToasts.length > 0) {
				emitToast.call(vueInstance, storedToasts[0].type, storedToasts[0].content);
				storedToasts.splice(0, 1);
			}
			shouldStoreToasts = false;
			Vue.toastComponentMountedCallback = undefined;
		}

		// The call back that the toast component can call
		Vue.toastComponentMountedCallback = switchToastMethod;

		Vue.prototype.$toast = function(type, content) {
			(shouldStoreToasts ? storeToast : emitToast).call(this, type, content);
		};
	}
}
