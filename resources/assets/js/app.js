
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// Google OAuth details
var clientId = '128388670916-c3ltv4i2hvqs4a3rlarru790m3505g0m.apps.googleusercontent.com';
var redirectUri = 'http://localhost:8000';

Vue.use(require('./plugins/Toasts.js').default);
Vue.use(require('./plugins/Auth.js').default, {
  clientId,
  redirectUri
});

Vue.component('taskList', require('./components/TaskList.vue'));
Vue.component('taskListItem', require('./components/TaskListItem.vue'));
Vue.component('newTaskBar', require('./components/NewTaskBar.vue'));
Vue.component('loadingIcon', require('./components/LoadingIcon.vue'));
Vue.component('toasts', require('./components/Toasts.vue'));
Vue.component('login', require('./components/Login.vue'));


const app = new Vue({
    el: '#app',
    mixins: [Vue.authMixin]
});
