
class Task {
	constructor(data) {
		this.id = data.id;
		this.name = data.name;
		this.createdAt = data.createdAt;
		this.updatedAt = data.updatedAt;
	}

	/**
	 * Saves this task to the database
	 */
	save() {
		return Vue.http.put('/api/tasks/' + this.id, {
			'name': this.name
		}, {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		})
			.then((updatedTask) => {
				this.updatedAt = updatedTask.updated_at;
			});
	}

	/**
	 * Deletes this task from the database
	 */
	delete() {
		return Vue.http.delete('/api/tasks/' + this.id);
	}
};


/**
 * Fetches all the tasks from the database and converts them to Task objects
 */
Task.listTasks = function() {
	return Vue.http.get('/api/tasks/')
		.then(function(response) {
			return response.body.map(function(taskBody) {
				return new Task(taskBody);
			});
		});
}


/**
 * Creates a new Task on the server and returns it.
 */
Task.createTask = function(name) {
	return Vue.http.put('/api/tasks/', {
		'name': name
	}, {
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
		.then(function(response) {
			var taskBody = {
				'id': response.body.id,
				'name': response.body.name,
				'createdAt': response.body.created_at,
				'updatedAt': response.body.updated_at,
			};
			return new Task(taskBody);
		});
}


export default Task;
